const User = require('../app/controllers/users');
const Chat = require('../app/controllers/chat');

module.exports = function(app) {
    app.get('/', function(req, res, nex) {
        res.json("Welcome to Chat API...!");
    });
    app.use('/api/user', User);
	app.use('/api/chat', Chat);
    
    //catch 404 and forward to error handler
    app.use(function(req, res, next) {
        res.status(404).render('404', { title: "Sorry, page not found", session: req.sessionbo });
    });

    app.use(function(req, res, next) {
        res.status(500).render('404', { title: "Sorry, page not found" });
    });
}