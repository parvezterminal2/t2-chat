class MessageService{
    constructor(){

    }

    addObject(param){
        return `${param} has been added successfully.`;
    }

    addObjectFail(param){
        return `${param} could not be added.`;
    }

    getObjects(param){
        return `${param} found successfully.`;
    }

    getObjectByID(param, id){
        return `${param} with ID ${id} found successfully.`;
    }

    notGetObjectByID(param, id){
        return `${param} with ID ${id} not found successfully.`;
    }

    updateObject(param, id){
        return `${param} with ID ${id} has been updated successfully.`;
    }

    updateDetails(param){
        return `${param} has been updated successfully.`;
    }

    updateObjectFail(param, id){
        return `${param} with ID ${id} could not be updated or does not exist.`;
    }

    objectsNotFound(param){
        return `Could not found ${param}.`;
    }

    objectNotFoundByID(param, id){
        return `Could not found ${param} with ID ${id}.`;
    }

    deleteObjectById(param, id){
        return `Deleted record of ${param} with ID ${id}.`;
    }

    payAndBook(param) {
        return `${param} is confirmed.`;
    }

    existsOrNot(param) {
        return `${param} already register with us as headquarter, Please register with us as branch`;
    }

    errorObject(param) {
        return `You entered ${param}`
    }

    userRegister(param) {
        return `${param} already register with us.`
    }

    loginUser(param) {
        return `${param} doesn't register with us. Please register with us.`
    }

    loginPassword() {
        return `Invalid UserName/Password.Please try to Login Again`
    }

    userLoginSuccess(param) {
        return `${param} login successfully.`
    }

    tableExists(param){
        return  `${param}.This table already exists in Database.`
    }
    dataExists(){
        return  `Already Set as Charge`
    }

  
    
}

module.exports.MessageService = MessageService;