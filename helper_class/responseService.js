class ResponseService {
    constructor(data = {}, message, success, statusCode) {
            this.data = data,
            this.message = message,
            this.success = success,
            this.statusCode = statusCode
    }

    get responseBody() {
        return {
            data: this.data,
            message: this.message,
            success: this.success
        }
    };
}

module.exports.ResponseService = ResponseService;