var db = require('../../config/database'); //reference of dbconnection.js  
var Basis = {  
    getAllBasis: function(callback) {  
        return db.query("select Id,Name,Code from BasisMaster order by Id", callback);  
    },  
    getBasisById: function(id, callback) {  
        return db.query("select Id, Name, Code from BasisMaster where Id=?", [id], callback);  
    },  
    getBasisByNameorCode: function(searchName, callback) {  
        return db.query("SELECT Id, Name, Code FROM BasisMaster where Name like '%"+searchName+"%' or Code like'%"+searchName+"%'", [searchName], callback);  
    },  
    addBasis: function(Basis, callback) {  
        return db.query("Insert into basis values(?,?,?)", [Basis.Id, Basis.Title, Basis.Status], callback);  
    },  
    deleteBasis: function(id, callback) {  
        return db.query("delete from basis where Id=?", [id], callback);  
    },  
    updateBasis: function(id, Basis, callback) {  
        return db.query("update basis set Title=?,Status=? where Id=?", [Basis.Title, Basis.Status, id], callback);  
    }  
};  
module.exports = Basis;