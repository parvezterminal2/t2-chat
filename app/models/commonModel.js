const offersModel = require('./offersModel');
const { CommonModel } = require('./common');
var offers = require('../models/newoffers');
const originAir = require('../models/originAir');
const originLcl = require('../models/originLcl');
const common = {
    createOffer: function (req, IncrementalId, callback) {
        console.log('IncrementalId  ', IncrementalId)
        let commonModel = new CommonModel();
        let offerId = IncrementalId;
        let offerStr = "" + offerId;
        let pad = "100000";
        let offerNumber = pad.substring(0, pad.length - offerStr.length) + offerStr;
        let defaultNumber = "313131";
        let offer = 0, checkDigit;
        for (let i = 0; i < defaultNumber.length; i++) {
            offer = offer + (parseInt(offerNumber[i]) * parseInt(defaultNumber[i]));
            checkDigit = offer % (pad.length + 1);
        }
        let finalOfferNumber = req.body.OfferSchedule.Destination_Airport_Code + offerNumber + checkDigit;
        console.log('offerNumber  ', finalOfferNumber)

        offers.insertDataIntoOffer(req.body, IncrementalId, finalOfferNumber, (err, offerDetails) => {
            
            if (err) {
                callback(err);
            } else {
                offers.insertDataIntoOfferInfoForAudit(req.body,finalOfferNumber, (err, offerDetailsInAudit) => {
                    if (err) {
                        callback(err);
                    } else {
                        originAir.insertDataIntoOfferOriginCharges(req.body, IncrementalId, (err, offerOrigin) => {
                         
                            if (err) {
                                callback(err);
                            } else {
                                originAir.insertDataIntoOfferDestinationCharges(req.body, IncrementalId, (err, offerDest) => {
                                   
                                    if (err) {
                                        callback(err);
                                    } else {
                                        originAir.insertAdditionalDataIntoOfferOriginCharges(req.body, IncrementalId, (err, offerOriginAdd) => {
                                         
                                            if (err) {
                                                callback(err);
                                            } else {
                                                originAir.insertDataIntoAdditionalOfferDestinationCharges(req.body, IncrementalId, (err, offerDestAdd) => {
                                                   
                                                    if (err) {
                                                        callback(err);
                                                    } else if (req.body.OfferInfo.Air_Lcl == "AIR" || req.body.OfferInfo.Air_Lcl == "air") {
                                                        originAir.insertDataIntoFrieghtRates(req.body, IncrementalId, finalOfferNumber, (err, result) => {
                                                            let res = {};
                                                            res.OfferId = IncrementalId;
                                                            res.finalOfferNumber = finalOfferNumber
                                                            console.log("Res: ", res);
                                                            callback("", res);
                                                        })
                                                    } else {
                                                        originLcl.insertDataIntoOfferFreightRateLcl(req.body, IncrementalId, finalOfferNumber, (err, result) => {
                                                            let res = {};
                                                            res.OfferId = IncrementalId;
                                                            res.finalOfferNumber = finalOfferNumber
                                                            console.log("Res: ", res);
                                                            callback("", res);
                                                        })
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                });
            }
        });
    },

    updateOffer: function (req, callback) {
        let commonModel = new CommonModel();
        offersModel.updateOfferInfo(req.body, (err, updateOfferDetails) => {
            if (err) {
                callback(err);
            } else if (updateOfferDetails) {
                offersModel.SaveOfferInfoForAudit(req.body, req.body.OfferInfo.OfferId, req.body.OfferInfo.OfferNo, (err, offerDetailsInAudit) => {
                    if (err) {
                        callback(err);
                    } else if (offerDetailsInAudit) {
                        if (req.body.OriginCharges.length > 0) {
                            commonModel.saveMultiOriginCharges(req.body.OriginCharges, req.body.OfferInfo.OfferId).then((offerOriginChargesDetails) => {
                                commonModel.saveMultiOriginChargesMaster(req.body, "Origin").then((offerOriginChargesDetailsMaster) => {
                                    if (req.body.DestinationCharges.length > 0) {
                                        commonModel.saveMultiDestinationCharges(req.body.DestinationCharges, req.body.OfferInfo.OfferId).then((offerDestChargesDetails) => {
                                            commonModel.saveMultiDestinationChargesMaster(req.body, "Destination").then((offerDestChargesDetailsMaster) => {
                                                if (req.body.AdditionalServicesforOrigin.length > 0) {
                                                    commonModel.saveMultiOriginAdditionalService(req.body.AdditionalServicesforOrigin, req.body.OfferInfo.OfferId).then((offerOriginAdditionalService) => {
                                                        commonModel.saveMultiOriginAdditionalServiceMaster(req.body, "Origin").then((offerOriginAdditionalServiceMaster) => {
                                                            if (req.body.AdditionalServicesforDestination.length > 0) {
                                                                commonModel.saveMultiDestAdditionalService(req.body.AdditionalServicesforDestination, req.body.OfferInfo.OfferId).then((offerDestAdditionalService) => {
                                                                    commonModel.saveMultiDestAdditionalServiceMaster(req.body, "Destination").then((offerDestAdditionalServiceMaster) => {
                                                                        let res = {};
                                                                        res.OfferId = req.body.OfferInfo.OfferId;
                                                                        console.log("Res: ", res);
                                                                        callback("", res);
                                                                    }).catch((err) => {
                                                                        callback(err);
                                                                    });
                                                                }).catch((err) => {
                                                                    callback(err);
                                                                });
                                                            } else {
                                                                let res = {};
                                                                res.OfferId = req.body.OfferInfo.OfferId;
                                                                callback("", res);
                                                            }
                                                        }).catch((err) => {
                                                            callback(err);
                                                        });
                                                    }).catch((err) => {
                                                        callback(err);
                                                    });
                                                } else {
                                                    if (req.body.AdditionalServicesforDestination.length > 0) {
                                                        commonModel.saveMultiDestAdditionalService(req.body.AdditionalServicesforDestination, req.body.OfferInfo.OfferId).then((offerDestAdditionalService) => {
                                                            commonModel.saveMultiDestAdditionalServiceMaster(req.body, "Destination").then((offerDestAdditionalServiceMaster) => {
                                                                let res = {};
                                                                res.OfferId = req.body.OfferInfo.OfferId;
                                                                callback("", res);
                                                            }).catch((err) => {
                                                                callback(err);
                                                            });
                                                        }).catch((err) => {
                                                            callback(err);
                                                        });
                                                    } else {
                                                        let res = {};
                                                        res.OfferId = req.body.OfferInfo.OfferId;
                                                        callback("", res);
                                                    }
                                                }
                                            }).catch((err) => {
                                                callback(err);
                                            });
                                        }).catch((err) => {
                                            callback(err);
                                        });
                                    } else {
                                        if (req.body.AdditionalServicesforOrigin.length > 0) {
                                            commonModel.saveMultiOriginAdditionalService(req.body.AdditionalServicesforOrigin, req.body.OfferInfo.OfferId).then((offerOriginAdditionalService) => {
                                                commonModel.saveMultiOriginAdditionalServiceMaster(req.body, "Origin").then((offerOriginAdditionalServiceMaster) => {
                                                    if (req.body.AdditionalServicesforDestination.length > 0) {
                                                        commonModel.saveMultiDestAdditionalService(req.body.AdditionalServicesforDestination, req.body.OfferInfo.OfferId).then((offerDestAdditionalService) => {
                                                            commonModel.saveMultiDestAdditionalServiceMaster(req.body, "Destination").then((offerDestAdditionalServiceMaster) => {
                                                                let res = {};
                                                                res.OfferId = req.body.OfferInfo.OfferId;
                                                                callback("", res);
                                                            }).catch((err) => {
                                                                callback(err);
                                                            });
                                                        }).catch((err) => {
                                                            callback(err);
                                                        });
                                                    } else {
                                                        let res = {};
                                                        res.OfferId = req.body.OfferInfo.OfferId;
                                                        callback("", res);
                                                    }
                                                }).catch((err) => {
                                                    callback(err);
                                                });
                                            }).catch((err) => {
                                                callback(err);
                                            });
                                        } else {
                                            let res = {};
                                            res.OfferId = req.body.OfferInfo.OfferId;
                                            callback("", res);
                                        }
                                    }
                                }).catch((err) => {
                                    callback(err);
                                });
                            }).catch((err) => {
                                callback(err);
                            });
                        } else {
                            if (req.body.DestinationCharges.length > 0) {
                                commonModel.saveMultiDestinationCharges(req.body.DestinationCharges, req.body.OfferInfo.OfferId).then((offerDestChargesDetails) => {
                                    commonModel.saveMultiDestinationChargesMaster(req.body, "Destination").then((offerDestChargesDetailsMaster) => {
                                        if (req.body.AdditionalServicesforOrigin.length > 0) {
                                            commonModel.saveMultiOriginAdditionalService(req.body.AdditionalServicesforOrigin, req.body.OfferInfo.OfferId).then((offerOriginAdditionalService) => {
                                                commonModel.saveMultiOriginAdditionalServiceMaster(req.body, "Origin").then((offerOriginAdditionalServiceMaster) => {
                                                    if (req.body.AdditionalServicesforDestination.length > 0) {
                                                        commonModel.saveMultiDestAdditionalService(req.body.AdditionalServicesforDestination, req.body.OfferInfo.OfferId).then((offerDestAdditionalService) => {
                                                            commonModel.saveMultiDestAdditionalServiceMaster(req.body, "Destination").then((offerDestAdditionalServiceMaster) => {
                                                                let res = {};
                                                                res.OfferId = req.body.OfferInfo.OfferId;
                                                                callback("", res);
                                                            }).catch((err) => {
                                                                callback(err);
                                                            });
                                                        }).catch((err) => {
                                                            callback(err);
                                                        });
                                                    } else {
                                                        let res = {};
                                                        res.OfferId = req.body.OfferInfo.OfferId;
                                                        callback("", res);
                                                    }
                                                }).catch((err) => {
                                                    callback(err);
                                                });
                                            }).catch((err) => {
                                                callback(err);
                                            });
                                        } else {
                                            let res = {};
                                            res.OfferId = req.body.OfferInfo.OfferId;
                                            callback("", res);
                                        }
                                    }).catch((err) => {
                                        callback(err);
                                    });
                                }).catch((err) => {
                                    callback(err);
                                });
                            } else {
                                let res = {};
                                res.OfferId = req.body.OfferInfo.OfferId;
                                callback("", res);
                            }
                        }
                    } else {
                        let res = {};
                        callback("", res);
                    }
                });
            } else {
                let res = {};
                callback("", res);
            }
        });
    },
};
module.exports = common;