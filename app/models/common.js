'use strict';
const offersModel = require('./newoffers');
//const bookingModel = require('./bookingModel');
const moment = require('moment');
const jwt = require('jwt-simple');
// const forwarder = require('../models/forwarderModel');
const _ = require('lodash')
class CommonModel {
    constructor(){
    }

    //Get counter count
    getCounter(){
        return new Promise((resolve, reject)=>{
            offersModel.getOfferCounter((err, offerCount) => {
                if(err) {
                    reject(err);
                } else {
                    resolve(offerCount);
                }
            });
        });
    }

    //Get counter count
    updateCounter(updateValue){
        return new Promise((resolve, reject)=>{
            let counterId = updateValue + 1;
            let incrementalId = updateValue + 1;
            offersModel.updateOfferCounter(counterId, incrementalId, (err, updateDetails) => {
                if(err) {
                    reject(err);
                } else {
                    resolve(updateDetails);
                }
            });
        });
    }

    saveMultiOriginCharges (originCharges, IncrementalId) {

        return new Promise((resolve, reject)=>{
            var OriginCharges = [];
            for (let i = 0; i < originCharges.length; i++) {
                if(originCharges[i].isInsert == true || originCharges[i].isInsert == 'true' || originCharges[i].isInsert == false || originCharges[i].isInsert == 'false') {
                    OriginCharges.push([IncrementalId,
                        originCharges[i].Code,
                        originCharges[i].Origin,
                        originCharges[i].ChargeName,
                        originCharges[i].Description,
                        originCharges[i].Unit,
                        originCharges[i].Rate,
                        originCharges[i].Tax,
                        originCharges[i].CurrencyId,
                        originCharges[i].CountryId,
                        originCharges[i].ForwarderName,
                        originCharges[i].ROE,
                        0
                    ]);
                }
            }
            if (OriginCharges.length > 0) {
                offersModel.SaveMultiOriginCharges(OriginCharges, false, (err, offerOriginChargesDetails) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(offerOriginChargesDetails);
                    }
                });
            } else {
                resolve({});
            }
        });   
    }

    saveMultiOriginChargesMaster (body, type) {
        return new Promise((resolve, reject)=>{
            let OriginChargesMaster = [], insertCount = 0, updateCount = 0, default_exist = "N";
            for (let i = 0; i < body.OriginCharges.length; i++) {
                if(body.OriginCharges[i].isInsert == true || body.OriginCharges[i].isInsert == 'true') {
                    OriginChargesMaster.push([type,
                        body.OfferInfo.Mode,
                        body.OriginCharges[i].Code,
                        body.OriginCharges[i].ChargeName,
                        body.OriginCharges[i].Description,
                        body.OriginCharges[i].Unit,
                        body.OriginCharges[i].CurrencyId,
                        body.OriginCharges[i].Rate,
                        body.OriginCharges[i].Tax,
                        body.OriginCharges[i].Comment,
                        body.OriginCharges[i].Reference,
                        default_exist,
                        body.CreatedBy,
                        body.ModifiedBy
                    ]);
                } else {
                    insertCount++;
                    updateCount++;
                    offersModel.updateChargeMasterData(body.OriginCharges[i], body, type, (err, offerOriginChargesDetailsMaster) => {
                        if (err) {
                            reject(err);
                        } else {
                            if (updateCount == body.OriginCharges.length) {
                                resolve(offerOriginChargesDetailsMaster);
                            }
                        }
                    });
                }
                
            }
            
            if (insertCount < body.OriginCharges.length) {
                offersModel.ChargeMaster(OriginChargesMaster, false, (err, offerOriginChargesDetailsMaster) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(offerOriginChargesDetailsMaster);
                    }
                });
            } else {
                resolve({});
            }
        });   
    }

    saveMultiDestinationCharges (destinationCharges, IncrementalId) {
        return new Promise((resolve, reject)=>{
            var DestinationCharges = [];
            for (let i = 0; i < destinationCharges.length; i++) {
                if(destinationCharges[i].isInsert == true || destinationCharges[i].isInsert == 'true' || destinationCharges[i].isInsert == false || destinationCharges[i].isInsert == 'false') {
                    DestinationCharges.push([IncrementalId,
                        destinationCharges[i].Code,
                        destinationCharges[i].ChargeName,
                        destinationCharges[i].Destination,
                        destinationCharges[i].Description,
                        destinationCharges[i].Unit,
                        destinationCharges[i].Rate,
                        destinationCharges[i].Tax,
                        destinationCharges[i].CurrencyId,
                        destinationCharges[i].CountryId,
                        destinationCharges[i].ForwarderName,
                        destinationCharges[i].ROE,
                        0
                    ]);
                }
            }

            if (DestinationCharges.length > 0) {
                offersModel.SaveMultiDestinationCharges(DestinationCharges, false, (err, offerDestChargesDetails) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(offerDestChargesDetails);
                    }
                });
            } else {
                resolve({});
            }
        });   
    }
    

    saveMultiDestinationChargesMaster (body, type) {
        return new Promise((resolve, reject)=>{
            let DestinationChargesMaster = [], insertCount = 0, updateCount = 0, default_exist = "N";
            for (let i = 0; i < body.DestinationCharges.length; i++) {
                if(body.DestinationCharges[i].isInsert == true || body.DestinationCharges[i].isInsert == 'true') {
                    DestinationChargesMaster.push([type,
                        body.OfferInfo.Mode,
                        body.DestinationCharges[i].Code,
                        body.DestinationCharges[i].ChargeName,
                        body.DestinationCharges[i].Description,
                        body.DestinationCharges[i].Unit,
                        body.DestinationCharges[i].CurrencyId,
                        body.DestinationCharges[i].Rate,
                        body.DestinationCharges[i].Tax,
                        body.DestinationCharges[i].Comment,
                        body.DestinationCharges[i].Reference,
                        default_exist,
                        body.CreatedBy,
                        body.ModifiedBy
                    ]);
                } else {
                    insertCount++;
                    updateCount++;
                    offersModel.updateChargeMasterData(body.DestinationCharges[i], body, type, (err, offerDestinationChargesDetailsMaster) => {
                        if (err) {
                            reject(err);
                        } else {
                            if (updateCount == body.DestinationCharges.length) {
                                resolve(offerDestinationChargesDetailsMaster);
                            }
                        }
                    });
                }
                
            }
            if (insertCount < body.DestinationCharges.length) {
                offersModel.ChargeMaster(DestinationChargesMaster, false, (err, offerDestMasterDetails) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(offerDestMasterDetails);
                    }
                });
            } else {
                resolve({});
            }
        });   
    }

    saveMultiOriginAdditionalService (AdditionalServicesforOrigin, IncrementalId) {
        return new Promise((resolve, reject)=>{
            var AddtionalOriginService = [];
            for (let i = 0; i < AdditionalServicesforOrigin.length; i++) {
                if(AdditionalServicesforOrigin[i].isInsert == true || AdditionalServicesforOrigin[i].isInsert == 'true' || AdditionalServicesforOrigin[i].isInsert == false || AdditionalServicesforOrigin[i].isInsert == 'false') {
                    AddtionalOriginService.push([IncrementalId,
                        AdditionalServicesforOrigin[i].Origin,
                        AdditionalServicesforOrigin[i].OriginName,
                        AdditionalServicesforOrigin[i].OriginDescription,
                        AdditionalServicesforOrigin[i].CountryId,
                        AdditionalServicesforOrigin[i].ForwarderName,
                        1
                    ]);
                }
            }

            if (AddtionalOriginService.length > 0) {
                offersModel.SaveMultiOriginCharges(AddtionalOriginService, true, (err, offerOriginAddSerDetails) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(offerOriginAddSerDetails);
                    }
                });
            } else {
                resolve({});
            }
        });   
    }

    saveMultiOriginAdditionalServiceMaster (body, type) {
        return new Promise((resolve, reject) => {
            var AddtionalOriginServiceMaster = [], insertCount = 0, updateCount = 0, default_exist = "N";
            for (let i = 0; i < body.AdditionalServicesforOrigin.length; i++) {
                if(body.AdditionalServicesforOrigin[i].isInsert == true || body.AdditionalServicesforOrigin[i].isInsert == 'true') {
                    AddtionalOriginServiceMaster.push([
                        type,
                        body.OfferInfo.Mode,
                        body.AdditionalServicesforOrigin[i].Code,
                        body.AdditionalServicesforOrigin[i].OriginName,
                        body.AdditionalServicesforOrigin[i].OriginDescription,
                        "",
                        "",
                        "",
                        "",
                        body.AdditionalServicesforOrigin[i].Comment,
                        body.AdditionalServicesforOrigin[i].Reference,
                        default_exist,
                        body.CreatedBy,
                        body.ModifiedBy
                    ]);
                } else {
                    insertCount++;
                    updateCount++;
                    offersModel.updateOriginServiceMasterData(body.AdditionalServicesforOrigin[i], body, type, (err, updateServiceMasterData) => {
                        if (err) {
                            reject(err);
                        } else {
                            if (updateCount == body.AdditionalServicesforOrigin.length) {
                                resolve(updateServiceMasterData);
                            }
                        }
                    });
                }
            }
            if (insertCount < body.AdditionalServicesforOrigin.length) {
                offersModel.ChargeMaster(AddtionalOriginServiceMaster, true, (err, offerOriginAddSerDetailsMaster) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(offerOriginAddSerDetailsMaster);
                    }
                });
            } else {
                resolve({});
            }
        });   
    }

    saveMultiDestAdditionalService (AdditionalServicesforDestination, IncrementalId) {
        return new Promise((resolve, reject)=>{
            let AddtionalDestService = [];
            for (let i = 0; i < AdditionalServicesforDestination.length; i++) {
                if(AdditionalServicesforDestination[i].isInsert == true || AdditionalServicesforDestination[i].isInsert == 'true' || AdditionalServicesforDestination[i].isInsert == false || AdditionalServicesforDestination[i].isInsert == 'false') {
                    AddtionalDestService.push([IncrementalId,
                        AdditionalServicesforDestination[i].Destination,
                        AdditionalServicesforDestination[i].DestinationName,
                        AdditionalServicesforDestination[i].DestinationDescription,
                        AdditionalServicesforDestination[i].CountryId,
                        AdditionalServicesforDestination[i].ForwarderName,
                        1
                    ]);
                }
            }
            if (AddtionalDestService.length > 0) {
                offersModel.SaveMultiDestinationCharges(AddtionalDestService, true, (err, offerDestAddiSerDetails) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(offerDestAddiSerDetails);
                    }
                });
            } else {
                resolve({});
            }
        });   
    }

    saveMultiDestAdditionalServiceMaster (body, type) {
        return new Promise((resolve, reject) => {
            var AddtionalDestServiceMaster = [], insertCount = 0, updateCount = 0, default_exist = "N";
            for (let i = 0; i < body.AdditionalServicesforDestination.length; i++) {
                if(body.AdditionalServicesforDestination[i].isInsert == true || body.AdditionalServicesforDestination[i].isInsert == 'true') {
                    AddtionalDestServiceMaster.push([
                        type,
                        body.OfferInfo.Mode,
                        body.AdditionalServicesforDestination[i].Code,
                        body.AdditionalServicesforDestination[i].DestinationName,
                        body.AdditionalServicesforDestination[i].DestinationDescription,
                        "",
                        "",
                        "",
                        "",
                        body.AdditionalServicesforDestination[i].Comment,
                        body.AdditionalServicesforDestination[i].Reference,
                        default_exist,
                        body.CreatedBy,
                        body.ModifiedBy
                    ]);
                } else {
                    insertCount++;
                    updateCount++;
                    offersModel.updateDestinationServiceMasterData(body.AdditionalServicesforDestination[i], body, type, (err, updateServiceMasterData) => {
                        if (err) {
                            reject(err);
                        } else {
                            if (updateCount == body.AdditionalServicesforDestination.length) {
                                resolve(updateServiceMasterData);
                            }
                        }
                    });
                }
            }
            if (insertCount < body.AdditionalServicesforDestination.length) {
                offersModel.SaveMultiDestinationChargesMaster(AddtionalDestServiceMaster, true, (err, offerDestAddiSerDetailsMaster) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(offerDestAddiSerDetailsMaster);
                    }
                });
            } else {
                resolve({});
            }
        });   
    }

    getAllOffersById(offerId, callback) {
        offersModel.getAllOffersbyID(offerId, (err, fullOfferDetails) => {
            if (err) {
                callback(err);
            } else {
                callback("", fullOfferDetails);
            }
        });
        //callback("", response);
    }

    createDefaultOriginDestinationAir(data, callback) {

        offersModel.createDefaultOriginDestinationAirFunc(defaultCharge, data.Origin_Destination, data.Air_Lcl, (err, createDefaultOriginAir) => {
            if (err) {
                callback(err, "");
            } else {
                callback("", createDefaultOriginAir)
            }
        });
    }

    createDefaultOriginDestinationLcl(data, callback) {
        let defaultCharge = [];
        for(let i = 0; i < data.Branch.length; i++) {
            for (let j = 0; j < data.Origin_Port_Code.length; j++) {
                for (let k = 0; k < data.Destination_Port_Code.length; k++) {
                    for (let l = 0; l < data.Origin_Port_Country_Code.length; l++) {
                        for (let m = 0; m < data.Destination_Port_Country_Code.length; m++) {
                            defaultCharge.push([
                                data.ChargeMasterId,
                                data.Charge_Master_Code,
                                data.Is_It_Default_Charge_Service,
                                data.Branch[i].Branch_Code,
                                data.Branch[i].Branch_Name,
                                data.Origin_Port_Country_Code[l].Code,
                                data.Origin_Port_Code[j].Code,
                                data.Destination_Port_Country_Code[m].Code,
                                data.Destination_Port_Code[k].Code,
                                data.Created_By,
                                data.Modified_By
                            ]);
                        }
                    }
                }
            }
        }
        offersModel.createDefaultOriginDestinationLclFunc(defaultCharge, data.Origin_Destination, data.Air_Lcl, (err, createDefaultOriginAir) => {
            if (err) {
                callback(err, "");
            } else {
                callback("", createDefaultOriginAir)
            }
        });
    }

    bookingOriginChargesFunc (originCharges, bookingData) {
        return new Promise((resolve, reject)=>{
            let OriginCharges = [];
            for (let i = 0; i < originCharges.length; i++) {
                OriginCharges.push([
                    bookingData.Booking_Id,
                    bookingData.Offer_Id,
                    "Charge",
                    originCharges[i].Code,
                    originCharges[i].Description,
                    originCharges[i].ChargeName,
                    originCharges[i].UnitId,
                    originCharges[i].CurrencyId,
                    originCharges[i].ROE,
                    originCharges[i].Rate,
                    originCharges[i].Comment,
                    originCharges[i].Reference1,
                    bookingData.CreatedBy,
                    bookingData.ModifiedBy
                ]);
            }
            bookingModel.bookingOriginCharges(OriginCharges, false, (err, offerOriginChargesDetails) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(offerOriginChargesDetails);
                }
            });
        });   
    }

    bookingOriginServicesFunc (originServices, bookingData) {
        return new Promise((resolve, reject)=>{
            let OriginServices = [];
            for (let i = 0; i < originServices.length; i++) {
                OriginServices.push([bookingData.BookingId,
                    bookingData.OfferId,
                    "Service",
                    originServices[i].Code,
                    originServices[i].ServiceName,
                    originServices[i].ServiceDesription,
                    originServices[i].Comment,
                    originServices[i].Reference1,
                    bookingData.CreatedBy,
                    bookingData.ModifiedBy
                ]);
            }
            bookingModel.bookingOriginCharges(OriginServices, true, (err, offerOriginServicesDetails) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(offerOriginServicesDetails);
                }
            });
        });   
    }

    bookingDestinationChargesFunc (originCharges, bookingData) {
        return new Promise((resolve, reject)=>{
            let OriginCharges = [];
            for (let i = 0; i < originCharges.length; i++) {
                OriginCharges.push([bookingData.BookingId,
                    bookingData.OfferId,
                    "Charge",
                    originCharges[i].Code,
                    originCharges[i].Description,
                    originCharges[i].ChargeName,
                    originCharges[i].UnitId,
                    originCharges[i].CurrencyId,
                    originCharges[i].ROE,
                    originCharges[i].Rate,
                    originCharges[i].Comment,
                    originCharges[i].Reference1,
                    bookingData.CreatedBy,
                    bookingData.ModifiedBy
                ]);
            }
            bookingModel.bookingDestinationCharges(OriginCharges, false, (err, offerOriginChargesDetails) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(offerOriginChargesDetails);
                }
            });
        });   
    }

    bookingDestinationServicesFunc (originServices, bookingData) {
        return new Promise((resolve, reject)=>{
            let OriginServices = [];
            for (let i = 0; i < originServices.length; i++) {
                OriginServices.push([bookingData.BookingId,
                    bookingData.OfferId,
                    "Service",
                    originServices[i].Code,
                    originServices[i].ServiceName,
                    originServices[i].ServiceDesription,
                    originServices[i].Comment,
                    originServices[i].Reference1,
                    bookingData.CreatedBy,
                    bookingData.ModifiedBy
                ]);
            }
            bookingModel.bookingDestinationCharges(OriginServices, true, (err, offerOriginServicesDetails) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(offerOriginServicesDetails);
                }
            });
        });   
    }

    randomJwtToken (data) {
        console.log("Data : ",data);
        let TOKEN_SECRET = process.env.TOKEN_SECRET || 'YOUR_UNIQUE_JWT_TOKEN_SECRET';
        data.exp = moment().add(1, 'days').unix();
        data.createdTime = moment().unix();
        return jwt.encode(data, TOKEN_SECRET);
    }

    ensureAuthenticated(req, res, next) {
        console.log("Request : ",req);
        let message = "";
	    let token = "";
        let payLoad = null;
        let TOKEN_SECRET = process.env.TOKEN_SECRET || 'YOUR_UNIQUE_JWT_TOKEN_SECRET'
        if (!req.header('Authorization')) {
			message = 'Please make sure your request has an Authorization header';
			return res.status(401).send({
                message: message,
                res: false
            });
		} else {
            token = req.header('Authorization').split(' ')[1];
            console.log("Token  : ",token);
			payLoad = jwt.decode(token, TOKEN_SECRET);
			if (payLoad != null) {
                console.log("Payload : ",payLoad);
				if (typeof payLoad.ForwarderUserId !== "undefined") {
					if (payLoad.exp <= moment().unix()) {
						message = 'Token has expired';
						return res.status(401).send({
                            message: message,
                            res: false
                        });
					} else {
                        
					}
				} else {
					message = "Invalid payload";
					return res.status(401).send({
                        message: message,
                        res: false
                    });
				}
			} else {
				message = "No payload";
				return res.status(401).send({
                    message: message,
                    res: false
                });
			}
		}
    }
};

module.exports.CommonModel = CommonModel;
