const express = require('express');
const router = express.Router();
const crypto = require('crypto');
const { MessageService } = require('../../helper_class/messages');
const { ResponseService } = require('../../helper_class/responseService');
const attachmentdata = require('../models/chatModel');
const moment = require('moment')

const algorithm = 'aes-256-cbc';
const key = crypto.randomBytes(32);
const iv = crypto.randomBytes(16);
const password = 'd6F3Efeq';
	
function encrypt(text){
	var cipher = crypto.createCipher(algorithm,password)
	var crypted = cipher.update(text,'utf8','hex')
	crypted += cipher.final('hex');
	return crypted;
}

function decrypt(text){
	var decipher = crypto.createDecipher(algorithm,password)
	var dec = decipher.update(text,'hex','utf8')
	dec += decipher.final('utf8');
	return dec;
}


router.post('/attachlist', (req, res) => {
    var data = {};
    var response = {};
    let responseService
    let messageService = new MessageService()
	
	let fromchat = req.body.fromchat;
    let tochat   = req.body.tochat;
	
    attachmentdata.getAllAttach(fromchat, tochat, (err, chatAttachDetails)=> {
        /*console.log('result of attachments ', chatAttachDetails);*/
		if (err) {  
            responseService = new ResponseService({}, err, false, 500);;
            return res.status(responseService.statusCode).send(responseService.responseBody); 
        } else {  
            responseService = new ResponseService(chatAttachDetails, messageService.getObjects('Attachment list'), true, 200);
            return res.status(responseService.statusCode).send(responseService.responseBody);
        }
    });
});


router.post('/grpattachlist', (req, res) => {
    var data = {};
    var response = {};
    let responseService
    let messageService = new MessageService()
	
	let grpid = req.body.grpid;
	
    attachmentdata.getAllGrpAttach(grpid, (err, grpChatAttachDetails)=> {
        /*console.log('result of attachments ', grpChatAttachDetails);*/
		if (err) {  
            responseService = new ResponseService({}, err, false, 500);;
            return res.status(responseService.statusCode).send(responseService.responseBody); 
        } else {  
            responseService = new ResponseService(grpChatAttachDetails, messageService.getObjects('Attachment list'), true, 200);
            return res.status(responseService.statusCode).send(responseService.responseBody);
        }
    });
});

router.post('/chathistory', (req, res) => {
    var data = {};
    var response = {};
    let responseService
    let messageService = new MessageService()
	
	let fromchat = req.body.fromchat;
    let tochat   = req.body.tochat;
	
    attachmentdata.getAllChat(fromchat, tochat, (err, chatHistory)=> {
        /*console.log('result of chat history ', chatHistory);*/
		if (err) {  
            responseService = new ResponseService({}, err, false, 500);;
            return res.status(responseService.statusCode).send(responseService.responseBody); 
        } else {
			var result_chat_data = [];
			var result_firstname = []
			var result_lastname  = [];
			for (var i = 0; i < chatHistory.length; i++) {
				result_chat_data.push(chatHistory[i].chat_data);
				result_firstname.push(chatHistory[i].firstname);
				result_lastname.push(chatHistory[i].lastname);
			}
			
			for (index = 0; index < chatHistory.length; index++) {
				chatHistory[index].chat_data = decrypt(result_chat_data[index])
				chatHistory[index].firstname = result_firstname[index]
				chatHistory[index].lastname  = result_lastname[index]
			}			
			responseService = new ResponseService(chatHistory, messageService.getObjects('Chat history'), true, 200);
			return res.status(responseService.statusCode).send(responseService.responseBody);
        }
    });
});


router.post('/grpchathistory', (req, res) => {
    var data = {};
    var response = {};
    let responseService
    let messageService = new MessageService()
	
	let grpid = req.body.grpid;
	let bychat = req.body.bychat;
	
    attachmentdata.getAllGrpChat(grpid, bychat, (err, grpChatHistory)=> {
        /*console.log('result of chat history ', grpChatHistory);*/
		if (err) {  
            responseService = new ResponseService({}, err, false, 500);;
            return res.status(responseService.statusCode).send(responseService.responseBody); 
        } else {
			attachmentdata.getAllGrpUsr(grpid, (err, grpChatUsrHistory)=> {
				if (err) {  
					responseService = new ResponseService({}, err, false, 500);;
					return res.status(responseService.statusCode).send(responseService.responseBody); 
				} else {
					var result_chat_data = [];
					var result_firstname = []
					var result_lastname  = [];
					for (var i = 0; i < grpChatHistory.length; i++) {
						result_chat_data.push(grpChatHistory[i].chat_data);
						result_firstname.push(grpChatHistory[i].firstname);
						result_lastname.push(grpChatHistory[i].lastname);
					}
					
					for (index = 0; index < grpChatHistory.length; index++) {
						grpChatHistory[index].chat_data = decrypt(result_chat_data[index])
						grpChatHistory[index].firstname = result_firstname[index]
						grpChatHistory[index].lastname  = result_lastname[index]
					}			
					responseService = new ResponseService({"Group_Chat_History": grpChatHistory, "Group_User_List": grpChatUsrHistory }, messageService.getObjects('Group chat history'), true, 200);
					return res.status(responseService.statusCode).send(responseService.responseBody);
					
					/*responseService = new ResponseService({ "Group_Chat_History": grpChatHistory, "Group_User_List": grpChatUsrHistory }, messageService.getObjects('Group chat history'), true, 200);
					return res.status(responseService.statusCode).send(responseService.responseBody);*/
				}
			});
        }
    });
});

router.post('/newchat',function(req,res,next){
	let messageService = new MessageService();
	/*console.log('chat-body ', req.body)*/
	
	const fromchat= req.body.fromchat;
	const tochat  = req.body.tochat;
	const chat	  = req.body.chat;
	
	let hw = encrypt(chat);
	/*console.log(hw);*/
	
	/*console.log(decrypt(hw));*/
	
	attachmentdata.chatInsert(fromchat, tochat, hw, (err, chatHistory) => {
		/*console.log('result of chat history ', chatHistory);*/
		if (err) {
			return db.rollback(() => {
				responseService = new ResponseService(err, messageService.objectsNotFound('Chat details'), false, 500);
				return res.status(responseService.statusCode).send(responseService.responseBody);
			});
		}else {
			attachmentdata.getAllChat(fromchat, tochat, (err, chatHistory)=> {
				/*console.log('result of chat history ', chatHistory);*/
				if (err) {  
					responseService = new ResponseService({}, err, false, 500);;
					return res.status(responseService.statusCode).send(responseService.responseBody); 
				} else {
					var result_chat_data = [];
					var result_firstname = []
					var result_lastname  = [];
					for (var i = 0; i < chatHistory.length; i++) {
						result_chat_data.push(chatHistory[i].chat_data);
						result_firstname.push(chatHistory[i].firstname);
						result_lastname.push(chatHistory[i].lastname);
					}
					
					for (index = 0; index < chatHistory.length; index++) {
                        chatHistory[index].chat_data = decrypt(result_chat_data[index])
                        chatHistory[index].firstname = result_firstname[index]
                        chatHistory[index].lastname  = result_lastname[index]
                    }			
					responseService = new ResponseService(chatHistory, messageService.getObjects('Chat history'), true, 200);
					return res.status(responseService.statusCode).send(responseService.responseBody);
				}
			});
		}
	});
});



router.post('/newgrpchat',function(req,res,next){
	let messageService = new MessageService()
	const grpid     = req.body.grpid;
	const bychat    = req.body.bychat;
	const chat_data = req.body.chat_data;
	let   hw        = encrypt(chat_data);
	
	attachmentdata.grpChatInsert(grpid, bychat, hw, (err, grpChatHistory) => {
		if (err) {
			return db.rollback(() => {
				responseService = new ResponseService(err, messageService.objectsNotFound('Chat details'), false, 500);
				return res.status(responseService.statusCode).send(responseService.responseBody);
			});
		}else {
			attachmentdata.getAllGrpChat(grpid, bychat, (err, grpChatHistory)=> {
				/*console.log('result of chat history ', grpChatHistory);*/
				if (err) {  
					responseService = new ResponseService({}, err, false, 500);;
					return res.status(responseService.statusCode).send(responseService.responseBody); 
				} else {
					var result_chat_data = [];
					var result_firstname = []
					var result_lastname  = [];
					for (var i = 0; i < grpChatHistory.length; i++) {
						result_chat_data.push(grpChatHistory[i].chat_data);
						result_firstname.push(grpChatHistory[i].firstname);
						result_lastname.push(grpChatHistory[i].lastname);
					}
					
					for (index = 0; index < grpChatHistory.length; index++) {
                        grpChatHistory[index].chat_data = decrypt(result_chat_data[index])
                        grpChatHistory[index].firstname = result_firstname[index]
                        grpChatHistory[index].lastname  = result_lastname[index]
                    }			
					responseService = new ResponseService(grpChatHistory, messageService.getObjects('Chat history'), true, 200);
					return res.status(responseService.statusCode).send(responseService.responseBody);	
					
					/*responseService = new ResponseService(grpChatHistory, messageService.getObjects('Chat history'), true, 200);
					return res.status(responseService.statusCode).send(responseService.responseBody);*/
				}
			});
		}
	});
});


router.post('/addnewgrp',function(req,res,next){
	let messageService = new MessageService()
	const usrid     = req.body.usrid;
	const groupname = req.body.groupname;
	
	attachmentdata.grpInsert(usrid, groupname, (err, result) => {
		let insertId = result.insertId
		
		if (err) {
			return db.rollback(() => {
				responseService = new ResponseService(err, messageService.objectsNotFound('Group details'), false, 500);
				return res.status(responseService.statusCode).send(responseService.responseBody);
			});
		}else {
			attachmentdata.usrGrpInsert(usrid, insertId, (err, result) => {
				if (err) {
					return db.rollback(() => {
						responseService = new ResponseService(err, messageService.objectsNotFound('Group details'), false, 500);
						return res.status(responseService.statusCode).send(responseService.responseBody);
					});
				}else {
					attachmentdata.getAllGrp(usrid, (err, grpList)=> {
						/*console.log('result of chat history ', result);*/
						if (err) {  
							responseService = new ResponseService({}, err, false, 500);;
							return res.status(responseService.statusCode).send(responseService.responseBody); 
						} else {  
							responseService = new ResponseService(grpList, messageService.getObjects('Group list'), true, 200);
							return res.status(responseService.statusCode).send(responseService.responseBody);
						}
					});
				}
			});
		}
	});
});

module.exports = router;
