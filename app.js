const express = require('express');
const app = express();
const constants = require('constants');
const constant = require('./config/constants');
const port = process.env.PORT || 8000;
const mysql = require('mysql');  
// const passport = require('passport');
// const flash = require('connect-flash');
var cors = require('cors')
app.use(cors())//enable all apis req and res
const path = require('path');
const morgan = require('morgan');
const bodyParser = require('body-parser');

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
// require('./config/passport')(passport); // pass passport for configuration
//set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(bodyParser()); // get information from html forms
//view engine setup
app.use(express.static(path.join(__dirname, './public/Web/dist')));
app.get(['/home', '/login', '/registration?*', '/origindefaults', '/chargemaster', '/permissions', '/companyinfo', '/myaccount', '/jobboard', '/offercreation', '/offercreation/newoffer',
        '/myoffers', '/allbookings', '/allpayments', '/originchargeslcl', '/destinationchargesair', '/destinationchargeslcl', '/marketplace', '/booking/*',
        '/bookingoffer/*', '/acceptance', '/sli', '/finalcharges', '/checkout', '/bookingdocs'],  (req, res) => {
        return res.sendFile(path.join(__dirname,'./public/Web/dist/index.html'));
});
//app.set('views', path.join(__dirname, 'app/views'));
//app.set('view engine', 'ejs');
//app.set('view engine', 'ejs'); // set up ejs for templating
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//Cron job
//require('./onServerStart/offerActiveLater.js')(app);
// routes ======================================================================
require('./config/routes.js')(app); // load our routes and pass in our app and fully configured passport

//launch ======================================================================
app.listen(port);
console.log('The magic happens on port ' + port);
exports = module.exports = app;