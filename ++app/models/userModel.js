var db = require('../../config/database'); //reference of dbconnection.js
const moment = require('moment')
date = moment(new Date()).format("YYYY-MM-DD HH:mm:ss") 
let UserMaster = {
	getUserData: (data, callback) => {
		/*console.log('userdetails ', JSON.stringify(data));*/
        let query = `select * from chatusers where username = ${JSON.stringify(data)}`;
        return db.query(query, callback);
		
    },
	
	getAllUser: function(callback) {  
        return db.query("select id, username from chatusers", callback);  
    },
	
	getAllGrp: function(usrid, callback) {  
        return db.query("select gm.grpid as id, ug.groupname FROM group_member as gm LEFT JOIN usrgroup ug ON gm.grpid = ug.id where gm.usrid IN ("+usrid+") ", callback);
    },
	
	getUrName: function(username, callback) {  
        return db.query("select id, username FROM chatusers where username LIKE '"+username+"%' ", callback);
    },
	
	getAllUsrOfGrp: function(groupid, callback) {  
        return db.query("select CONCAT(cu.firstname,' ',cu.lastname) as uname FROM group_member as gm LEFT JOIN chatusers cu ON gm.usrid = cu.id where gm.grpid IN ("+groupid+") ", callback);
    },
	
	addUserToGroup: (groupid, userid, callback) => {
        let query = `Insert into group_member
        (
            grpid,
            usrid
        ) values (?,?)`;

        let data = [
            groupid,
            userid
        ];
        return db.query(query, data, callback);
    }
};  
module.exports = UserMaster;