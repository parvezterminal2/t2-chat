var db = require('../../config/database'); //reference of dbconnection.js
const moment = require('moment')
date = moment(new Date()).format("YYYY-MM-DD HH:mm:ss") 
let ChatMaster = {
	getAllAttach: function(fromchat, tochat, callback) {
		return db.query("select ca.bychat, ca.attach_data, cu.firstname, cu.lastname FROM chatattachment as ca LEFT JOIN chatusers cu ON ca.bychat = cu.id where (ca.fromchat="+fromchat+" AND ca.tochat="+tochat+") OR (ca.fromchat="+tochat+" AND ca.tochat="+fromchat+")", callback);
    },
	
	getAllGrpAttach: function(grpid, callback) {
		return db.query("select cb.bychat, cb.attach_data, cu.firstname, cu.lastname FROM grpchatattachment as cb LEFT JOIN chatusers cu ON cb.bychat = cu.id where cb.grpid="+grpid, callback);
    },
	
	getAllChat: function(fromchat, tochat, callback) {
		return db.query("select cb.bychat, cb.chat_data, cu.firstname, cu.lastname FROM chatboat as cb LEFT JOIN chatusers cu ON cb.bychat = cu.id where (cb.fromchat="+fromchat+" AND cb.tochat="+tochat+") OR (cb.fromchat="+tochat+" AND cb.tochat="+fromchat+")", callback);
    },
	
	getAllGrpChat: function(grpid, bychat, callback) {
		return db.query("select cb.bychat, cb.chat_data, cu.firstname, cu.lastname FROM grpchatboat as cb LEFT JOIN chatusers cu ON cb.bychat = cu.id where cb.grpid="+grpid, callback);
    },
	
	getAllGrpUsr: function(grpid, callback) {
		return db.query("select CONCAT(cu.firstname,' ',cu.lastname) as uname FROM group_member as gm LEFT JOIN chatusers cu ON gm.usrid = cu.id where grpid="+grpid, callback);
    },
	
	getAllGrp: function(usrid, callback) {  
        return db.query("select gm.grpid as id, ug.groupname FROM group_member as gm LEFT JOIN usrgroup ug ON gm.grpid = ug.id where gm.usrid IN ("+usrid+") ", callback);
    },
	
	chatInsert: (fromchat, tochat, chat, callback) => {
        /*console.log('request body    ', chat)*/
        let query = `Insert into chatboat
        (
            fromchat,
            tochat, 
            bychat, 
            chat_data 
        ) values (?,?,?,?)`;

        let data = [
            fromchat,
            tochat,
            fromchat,
            chat
        ];
        return db.query(query, data, callback);
    },
	
	grpChatInsert: (grpid, bychat, chat_data, callback) => {
        /*console.log('request body    ', chat)*/
        let query = `Insert into grpchatboat
        (
            grpid,
            bychat, 
            chat_data 
        ) values (?,?,?)`;

        let data = [
            grpid,
            bychat,
            chat_data
        ];
        return db.query(query, data, callback);
    },
	
	grpInsert: (usrid, groupname, callback) => {
        let query = `Insert into usrgroup
        (
            groupname 
        ) values (?)`;

        let data = [
            groupname
        ];
        return db.query(query, data, callback);
    },
	
	usrGrpInsert: (usrid, grpid, callback) => {
        let query = `Insert into group_member
        (
            grpid,
            usrid
        ) values (?,?)`;

        let data = [
            grpid,
            usrid
        ];
        return db.query(query, data, callback);
    }
};  
module.exports = ChatMaster;