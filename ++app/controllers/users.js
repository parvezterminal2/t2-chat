const express = require('express');
const router = express.Router();
const { MessageService } = require('../../helper_class/messages');
const { ResponseService } = require('../../helper_class/responseService');
const userdata = require('../models/userModel');
const moment = require('moment')

router.post('/login', (req, res) => {
    var data = {};
    var response = {};
    let responseService
    let messageService = new MessageService()	
	let pass     = req.body.password;
    let username = req.body.username;
	
    userdata.getUserData(username, (err, getUserDetails)=> {
        if (err){
            responseService = new ResponseService({}, err, false, 500)
            return res.status(responseService.statusCode).send(responseService.responseBody)
        } else {
            let pass_db = getUserDetails[0].password
			let user_db = getUserDetails[0].username
            let newpass = pass
			
			if (pass_db == newpass && user_db == username) {
                data.first_name = getUserDetails[0].firstname
				data.last_name  = getUserDetails[0].lastname
				data.username   = getUserDetails[0].username
				data.userid     = getUserDetails[0].id
				
				responseService = new ResponseService(data, messageService.userLoginSuccess('User'), true, 200);
				return res.status(responseService.statusCode).send(responseService.responseBody);
            } else {
				responseService = new ResponseService(err, messageService.objectsNotFound('user details'), false, 500);
				return res.status(responseService.statusCode).send(responseService.responseBody);
            }
        }
    });
});


router.post('/userlist', (req, res) => {
	let responseService;
    let messageService = new MessageService(); 
    userdata.getAllUser(function(err, userList) {
		if (err) {  
            responseService = new ResponseService({}, err, false, 500);;
            return res.status(responseService.statusCode).send(responseService.responseBody); 
        } else {  
            responseService = new ResponseService(userList, messageService.getObjects('Userlist'), true, 200);
            return res.status(responseService.statusCode).send(responseService.responseBody);
        }  
    });
});

router.post('/grplist', (req, res) => {
	let usrid = req.body.usrid;
	let responseService;
    let messageService = new MessageService(); 
    userdata.getAllGrp(usrid, function(err, grpList) {
		if (err) {  
            responseService = new ResponseService({}, err, false, 500);;
            return res.status(responseService.statusCode).send(responseService.responseBody); 
        } else {  
            responseService = new ResponseService(grpList, messageService.getObjects('grpList'), true, 200);
            return res.status(responseService.statusCode).send(responseService.responseBody);
        }  
    });
});

router.post('/getusrnameforgroup', (req, res) => {
	let username = req.body.username;
	let responseService;
    let messageService = new MessageService(); 
    userdata.getUrName(username, function(err, grpList) {
		/*console.log('middle')*/
        if (err) {  
            responseService = new ResponseService({}, err, false, 500);;
            return res.status(responseService.statusCode).send(responseService.responseBody); 
        } else {  
            responseService = new ResponseService(grpList, messageService.getObjects('User list by searching'), true, 200);
            return res.status(responseService.statusCode).send(responseService.responseBody);
        }  
    });
});

router.post('/addusrnametogroup', (req, res) => {
	let groupid = req.body.groupid;
	let userid  = req.body.userid;
	let responseService;
    let messageService = new MessageService();

	userdata.getAllGrp(userid, (err, grpChatUsr)=> {
		/*console.log(grpChatUsr)*/
		if (err) {  
			responseService = new ResponseService({}, err, false, 500);;
			return res.status(responseService.statusCode).send(responseService.responseBody); 
		} else {  
			if(grpChatUsr!=''){
				userdata.getAllUsrOfGrp(groupid, (err, grpChatUsr)=> {
					if (err) {  
						responseService = new ResponseService({}, err, false, 500);;
						return res.status(responseService.statusCode).send(responseService.responseBody); 
					} else {  
						responseService = new ResponseService({ "Group_User_List": grpChatUsr }, messageService.getObjects('Group chat list'), true, 200);
						return res.status(responseService.statusCode).send(responseService.responseBody);
					}
				});
			}else{
				userdata.addUserToGroup(groupid, userid, function(err, grpList) {
					/*console.log(grpList)*/
					if (err) {  
						responseService = new ResponseService({}, err, false, 500);;
						return res.status(responseService.statusCode).send(responseService.responseBody); 
					} else {
						userdata.getAllUsrOfGrp(groupid, (err, grpChatUsr)=> {
							if (err) {  
								responseService = new ResponseService({}, err, false, 500);;
								return res.status(responseService.statusCode).send(responseService.responseBody); 
							} else {  
								responseService = new ResponseService({ "Group_User_List": grpChatUsr }, messageService.getObjects('Group chat list'), true, 200);
								return res.status(responseService.statusCode).send(responseService.responseBody);
							}
						});
					}
				});
			}
			/*responseService = new ResponseService({ "Group_User_List": grpChatUsr }, messageService.getObjects('Group chat list'), true, 200);
			return res.status(responseService.statusCode).send(responseService.responseBody);*/
		}
	});	
});

module.exports = router;
