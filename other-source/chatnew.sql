-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 23, 2019 at 12:10 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chatnew`
--

-- --------------------------------------------------------

--
-- Table structure for table `chatattachment`
--

DROP TABLE IF EXISTS `chatattachment`;
CREATE TABLE IF NOT EXISTS `chatattachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fromchat` int(11) NOT NULL,
  `tochat` int(11) NOT NULL,
  `bychat` int(11) NOT NULL,
  `attach_data` text NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chatboat`
--

DROP TABLE IF EXISTS `chatboat`;
CREATE TABLE IF NOT EXISTS `chatboat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fromchat` int(11) NOT NULL,
  `tochat` int(11) NOT NULL,
  `bychat` int(11) NOT NULL,
  `hw` text NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chatboat`
--

INSERT INTO `chatboat` (`id`, `fromchat`, `tochat`, `bychat`, `hw`, `created_on`) VALUES
(1, 1, 2, 1, 'af8fb62a5fb8ac3054ffeed084331403', '2019-12-23 16:52:19'),
(2, 2, 1, 2, '1e895a8c3871b73542285d8e45b442eb', '2019-12-23 16:52:35');

-- --------------------------------------------------------

--
-- Table structure for table `chatusers`
--

DROP TABLE IF EXISTS `chatusers`;
CREATE TABLE IF NOT EXISTS `chatusers` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chatusers`
--

INSERT INTO `chatusers` (`id`, `username`, `password`, `firstname`, `lastname`, `status`) VALUES
(1, 'user1', '123456', 'suresh', 'kumar', 1),
(2, 'user2', '123456', 'Parvez', 'Alam', 1),
(3, 'user3', '123456', 'Riz', 'Alam', 1),
(4, 'user4', '123456', 'Faiz', 'Alam', 1),
(5, 'user5', '123456', 'Rohit', 'Sharma', 1);

-- --------------------------------------------------------

--
-- Table structure for table `group_member`
--

DROP TABLE IF EXISTS `group_member`;
CREATE TABLE IF NOT EXISTS `group_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grpid` int(11) NOT NULL,
  `usrid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_member`
--

INSERT INTO `group_member` (`id`, `grpid`, `usrid`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 1),
(5, 2, 2),
(20, 3, 4),
(19, 3, 5),
(18, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `grpchatattachment`
--

DROP TABLE IF EXISTS `grpchatattachment`;
CREATE TABLE IF NOT EXISTS `grpchatattachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grpid` int(11) NOT NULL,
  `bychat` int(11) NOT NULL,
  `attach_data` text NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `grpchatboat`
--

DROP TABLE IF EXISTS `grpchatboat`;
CREATE TABLE IF NOT EXISTS `grpchatboat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grpid` int(11) NOT NULL,
  `bychat` int(11) NOT NULL,
  `hw` text NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grpchatboat`
--

INSERT INTO `grpchatboat` (`id`, `grpid`, `bychat`, `hw`, `created_on`) VALUES
(1, 1, 1, '774718779a747c72a1db1e858c2bf94c', '2019-12-23 16:52:51'),
(2, 1, 2, '7952f035c2b59d76554aef98b52dbc25d662f690930eb37646fecd3904778766', '2019-12-23 16:53:47'),
(3, 1, 1, '0215e87b8698950e771d02f48512ae76', '2019-12-23 16:54:01'),
(4, 2, 1, 'bd81dbc2ec627d80be29dce20c373aedfb5cdc61c6efe226c6ce668d4f3fde97', '2019-12-23 16:54:29'),
(5, 2, 2, 'f9a8b74cd2f7cab8affd3dfcb4b7a5832fa7bdf9268bed23fe6d8441c157f65a', '2019-12-23 16:54:46');

-- --------------------------------------------------------

--
-- Table structure for table `usrgroup`
--

DROP TABLE IF EXISTS `usrgroup`;
CREATE TABLE IF NOT EXISTS `usrgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupname` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usrgroup`
--

INSERT INTO `usrgroup` (`id`, `groupname`, `status`) VALUES
(1, 'grp1', 1),
(2, 'grp2', 1),
(3, 'grpnew', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
